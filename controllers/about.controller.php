<?php


class AboutController extends Controller {
    public const ROUTE = "acercade";

    public const INDEX = "index";
    public const MANAGE = "manage";

    public $actions;

    public function __construct(){
        //parent::__construct($pathName);

        $this->actions = [
                            self::INDEX => Action::Default("Index"),
                            self::MANAGE => Action::Post("Manage")
                        ];
    }

 
    public function ShowContent($paths) {
        //Determine wich method call
        Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths){
        echo "<div class='container mt-4'><h1>Soy la pagina de Acerca de</h1></div>";

    }

    public function Manage($paths){

        echo "Soy la peticion post de about ".$_POST["parameter"];


    }


}