<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";
require_once "helpers/action.php";

class UsersController extends Controller {
    //CONTROLLER ROUTE
    public const ROUTE = "usuarios";

    //ACTIONS ROUTING
    public const SHOW_ALL_USERS = "mostrar";
    public const EDIT_USER = "editarusuario";
    public const INDEX = "index";
    public const DELETE = "deleteuser";
    public const AJAX = "ajax";
    public const REST = 'rest';


    public $actions;

    public function __construct(){
        //parent::__construct($pathName);

        $this->actions = [self::SHOW_ALL_USERS=> Action::Default("ShowAllUsers"),
                                self::EDIT_USER => Action::Default("EditUser", "EditUserPost"),
                                self::INDEX => Action::Default("Index", "IndexPost"),
                                self::DELETE => Action::Post("DeleteUser"),
                                self::AJAX => Action::Post("Ajax"),
                                //Instead of using separeted methods, we need to use Default method to set all the posible verbs
                                self::REST => Action::Default('GetTest',  'PostTest', 'PutTest', 'DeleteTest'),
                                ];
    }

    public function ShowContent($paths) {
        //Determine wich method call TO A SPECIFIC ACTION SENDED IT IN THE URL
        Action::ValidateActionsPath($paths, $this);
    }


    public function Index($paths){       

        $test = 20;
       include "views/users/index.php";
    }

    
    public function IndexPost($paths) {
        
        $user = new UserModel();
        $user->setName($_POST["name"]);
        $user->setLastName($_POST["lastname"]);
        $user->setEmail($_POST["email"]);

        
        $result = $user->InsertUser($user);


        
        $args = ["result" => $result];

        include "views/users/index.php";            
       
    }

    public function ShowAllUsers($paths){
        $allUsers = UserModel::GetAllUsers();

        include "views/users/showallusers.php";
    }

    public function EditUser($paths){

        if(isset($paths[2])){
            
            $user = UserModel::GetUserById($paths[2]);


            include "views/users/edituser.php";

        } else{
            include "views/error.php";
        }

    }

    public function EditUserPost($paths){
        
        $idToEdit = $_POST["id"];

        $user = UserModel::GetUserById($idToEdit);
        
        $user->setName($_POST["name"]);
        $user->setLastName($_POST["lastname"]);
        $user->setEmail($_POST["email"]);

        if($user->UpdateUser()){
            header("Location: /semiframework/".self::ROUTE."/".self::SHOW_ALL_USERS);
            die();
        } else {
            include "views/error.php";
        }

        
    }

    public function DeleteUser($paths){
        $idToDelete = $_POST["deleteId"];

        $result = UserModel::DeleteUserById($idToDelete);

        if($result){
            header("Location: /semiframework/".self::ROUTE."/".self::SHOW_ALL_USERS);
            die();
        } else {
            include "views/error.php";
        }

    }

    public function GetTest() {
        ob_end_clean();

        header('Content-Type: Application/Json');

        echo json_encode([
            "STATUS" => "SUCCESS",
            "MESSAGE" => "Everything was succesfull this is a GET request"
        ]);
        
       
        die();
    }

    public function PostTest() {
        ob_end_clean();

        header('Content-Type: Application/Json');

        echo json_encode([
            "STATUS" => "SUCCESS",
            "MESSAGE" => "Everything was succesfull this is a POST request"
        ]);

        die();
    }

    public function PutTest() {
        ob_end_clean();


        header('Content-Type: Application/Json');

        echo json_encode([
            "STATUS" => "SUCCESS",
            "MESSAGE" => "Everything was succesfull this is a PUT request"
        ]);

        die();
    }

    public function DeleteTest() {
        ob_end_clean();


        header('Content-Type: Application/Json');

        echo json_encode([
            "STATUS" => "SUCCESS",
            "MESSAGE" => "Everything was succesfull this is a DELETE request"
        ]);

        die();
    }

    public function Ajax($paths){
        ob_end_clean();

        
        $name = $_POST["name"];
        $age = $_POST["age"];

        // Print any response in JSON format

       


        $response = ["response" => "I'm the response"];
        echo json_encode($response);
        die();
    }

 
}
