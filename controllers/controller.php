<?php



abstract class Controller {
    

    protected $response;

    public function __construct(){
        //$this->pathName = strtolower($pathName);
        $this->response = new ResponseHandler();
    }

    

    public static function ValidateControllersPath($controllers){
        if(isset($_GET["page"])){
            
            $paths = explode("/", strtolower($_GET["page"]));
            if(isset($paths[0])){
                
                // Setting the get variables depending on the url query
                $url = parse_url($_SERVER['REQUEST_URI']);
                parse_str($url["query"] ?? null, $_GET);

                if(isset($controllers[$paths[0]])){
                    $controllers[$paths[0]]->ShowContent($paths);
                    return;
                } 

                $errorCode = 404;
                $errorMessage = "Parece que no hemos encontrado la pagina.";
                include 'views/error.php';
                
            } else {
                $controllers[HomeController::ROUTE]->ShowContent($paths);
            }

            return; 
        }
        

        $controllers[HomeController::ROUTE]->ShowContent(null);

    }

    
    abstract public function ShowContent($paths);
    abstract public function Index($paths);

}