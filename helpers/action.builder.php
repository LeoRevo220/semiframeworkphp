<?php 

require_once "action.php";


class ActionBuilder {
    private $getCallback;
    private $postCallback;
    private $putCallback;
    private $deleteCallback;


    private function __construct() {

    }

    public static function Builder() {
        return new self();
    }

    public function Get($getCallback) {
        $this->getCallback = $getCallback;

        return $this;
    }

    public function Post($postCallback) {
        $this->postCallback = $postCallback;
        return $this;
    }

    public function Put($putCallback) {
        $this->putCallback = $putCallback;
        return $this;
    }

    public function Delete($deleteCallback) {
        $this->deleteCallback = $deleteCallback;
        return $this;
    }

    public function Build() {
        $action = Action::Default($this->getCallback, $this->postCallback, $this->putCallback, $this->deleteCallback);

        return $action;
    }


}