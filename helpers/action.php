<?php 

require_once "action.builder.php";

class Action {


    private $getCallback;
    private $postCallback;
    private $putCallback;
    private $deleteCallback;

    private function __construct($getCallback, $postCallback = null, $putCallback = null, $deleteCallback = null){
        $this->getCallback = $getCallback;
        $this->postCallback = $postCallback;
        $this->putCallback = $putCallback;
        $this->deleteCallback = $deleteCallback;
    }

    public static function Get($getCallback){
        $action = new Action($getCallback);

        return $action;
    }
    
    public static function Post($postCallback){
        $action = new Action(null, $postCallback);

        return $action;
    }

    public static function Put($putCallback){
        $action = new Action(null,null,$putCallback);

        return $action;
    }

    public static function Delete($deleteCallback){
        $action = new Action(null,null,null,$deleteCallback);

        return $action;
    }

    public static function Default($getCallback, $postCallback=null, $putCallback = null, $deleteCallback = null) {
        $action = new Action($getCallback, $postCallback, $putCallback, $deleteCallback);

        return $action;
    }

    public static function Builder() {
        return ActionBuilder::Builder();
    }

    public function Invoke($controller, $paths){
        try {

            if($_SERVER["REQUEST_METHOD"] == "GET"){
                call_user_func([$controller, $this->getCallback], $paths);
                return;
            } 
            if($_SERVER["REQUEST_METHOD"] == "POST"){
                call_user_func([$controller, $this->postCallback], $paths);
                return;
            }
            if($_SERVER["REQUEST_METHOD"] == "PUT") {
                call_user_func([$controller, $this->putCallback], $paths);
                return;
            } 
            if($_SERVER["REQUEST_METHOD"] == "DELETE") {
                call_user_func([$controller, $this->deleteCallback], $paths);
                return;
            }

        } catch(Throwable $exception){
            $this->ResponseError($exception);
        }
        

    }

    public function ResponseError($exception) {
        ob_end_clean();

        http_response_code(500);
        header('Content-Type: Application/Json');

        echo json_encode([
            'STATUS' => 'ERROR',
            'MESSAGE' => 'Something got wrong',
            'EXCEPTION' => $exception->getMessage()
        ]);

        die();
    }

    public static function ValidateActionsPath($paths, $controller){
        
        //It's always expecting for a index method to be call in the default scenario when nothing is sent to the controller
        if(isset($paths[1])){
            if(isset($controller->actions[$paths[1]])){
                $controller->actions[$paths[1]]->Invoke($controller, $paths);
                return;
            }
            $controller->actions["index"]->Invoke($controller, $paths);
        } else {
            $controller->actions["index"]->Invoke($controller, $paths);
        }


    }










}